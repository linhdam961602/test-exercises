if (typeof window.Steenify === 'undefined') {
  window.Steenify = {};
}
window.Steenify = $.extend(Steenify, {
  init__home: function() {
    Steenify.reviews__slider__home();
    Steenify.delay__animation();
  },
  delay__animation: function() {
    const dtDelay = $('[data-delay]');

    $.each(dtDelay, function() {
      let self = $(this);
      let timeDelay = self.attr('data-delay');

      self.css({
        'animation-delay': timeDelay + 's',
      });
    });
  },
  reviews__slider__home: function reviews__slider__home() {
    const slider = $('[data-slider]');
    const sliderPrev = $('[data-btn-prev]');
    const sliderNext = $('[data-btn-next]');

    slider.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: true,
      infinite: false,
      centerPadding: '80px',
      prevArrow: sliderPrev,
      nextArrow: sliderNext,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            arrows: false,
            dots: false,
          },
        },
      ],
    });
  },
});
