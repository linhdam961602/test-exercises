// import Swup from 'swup';
// import swupMergeHeadPlugin from 'swup/plugins/swupMergeHeadPlugin';
// import AOS from 'aos';
// Import css
import './scss/index.scss';

if (typeof window.Steenify === 'undefined') {
  window.Steenify = {};
}

import './js/home';

window.Steenify = $.extend(Steenify, {
  init: function() {
    Steenify.init__page();
    Steenify.init__full__page();
    Steenify.init__resize();
  },
  init__page: function() {
    const view = $('.steenify__page').data('view');
    switch (view) {
      case 'home':
        Steenify.init__home();
        break;
      case 'about':
        Steenify.init__about();
        break;
      case 'turn__me':
        Steenify.init__turn__me();
        break;
      default:
        console.log(view);
    }
  },
  init__resize: function() {
    let resizeTimer;

    $(window).on('resize', function(e) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
        Steenify.init__full__page();
      }, 250);
    });
  },
  init__full__page: function() {
    const fullPage = $('[data-full-page]');
    let wW = window.innerWidth;

    if ($('html').hasClass('fp-enabled')) {
      $.fn.fullpage.destroy('all');
    }
    if (wW >= 767) {
      fullPage.fullpage({
        autoScrolling: true,
        scrollHorizontally: true,
        scrollingSpeed: 1000,
        afterLoad: function(origin, destination, direction) {
          setTimeout(function() {
            $('.section').removeClass('in-show');
            $('.section')
              .eq(destination - 1)
              .addClass('in-show');
          }, 100);
        },
      });
    }
  },
});

//***************************************
//      Main program
//***************************************

$(document).ready(function(event) {
  Steenify.init();
});
